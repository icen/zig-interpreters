const std = @import("std");
const Chunk = @import("./chunk.zig").Chunk;
const v = @import("./value.zig");
const Op = @import("./defs.zig").Op;
const M = @import("./main.zig");

pub const VM = struct {
    chunk: *const Chunk,
    ip: usize,
    jumps: std.ArrayList(usize),
    values: std.ArrayList(v.Value),

    pub fn init(alloc: std.mem.Allocator) !VM {
        return VM{ .chunk = undefined, .ip = 0, .jumps = std.ArrayList(usize).init(alloc), .values = std.ArrayList(v.Value).init(alloc) };
    }

    pub fn deinit(self: VM) void {
        self.jumps.deinit();
        self.values.deinit();
    }

    pub fn interpret(self: *VM, chunk: *const Chunk) !void {
        self.chunk = chunk;
        self.ip = 0;
        try self.run();
    }

    fn run(self: *VM) !void {
        while (self.ip < self.chunk.code.items.len) {
            const op = self.chunk.code.items[self.ip].opcode;
            std.debug.print("ip: {d} op: {} stack: {any}\n", .{ self.ip, op, self.values.items });
            self.ip += 1;
            switch (op) {
                .trap => return error.TrapOpcode,
                .ret => if (self.jumps.items.len > 0) {
                    self.ip = self.jumps.pop();
                } else {
                    return;
                },
                .neg => {
                    const x = self.values.pop();
                    if (x.is(f64)) {
                        try self.values.append(v.Value.from_f64(-x.as(f64)));
                    } else {
                        return error.TypeError;
                    }
                },
                .not => {
                    const x = self.values.pop();
                    try self.values.append(v.Value.from_bool(!v.truthy(x)));
                },
                .eq => {
                    const y = self.values.pop();
                    const x = self.values.pop();
                    try self.values.append(v.Value.from_bool(v.Value.eq(x, y)));
                },
                .add => try self.binary(.add),
                .sub => try self.binary(.sub),
                .mul => try self.binary(.mul),
                .div => try self.binary(.div),
                .gt => try self.binary(.gt),
                .lt => try self.binary(.lt),
                .print => {
                    var out = std.io.getStdOut().writer();
                    try out.print("{d}\n", .{self.values.items[self.values.items.len - 1]});
                },
                .constant => {
                    try self.values.append(self.chunk.constants.items[self.chunk.code.items[self.ip].index]);
                    self.ip += 1;
                },
                else => return error.UnknownOpcode,
            }
        }
    }

    inline fn binary(self: *VM, comptime op: Op) !void {
        const y = self.values.pop();
        const x = self.values.pop();
        if (x.is(f64) and y.is(f64)) {
            try self.values.append(switch (op) {
                .add => v.Value.from_f64(x.as(f64) + y.as(f64)),
                .sub => v.Value.from_f64(x.as(f64) - y.as(f64)),
                .mul => v.Value.from_f64(x.as(f64) * y.as(f64)),
                .div => v.Value.from_f64(x.as(f64) / y.as(f64)),
                .lt => v.Value.from_bool(x.as(f64) < y.as(f64)),
                .gt => v.Value.from_bool(x.as(f64) > y.as(f64)),
                else => unreachable,
            });
        } else if (op == .add) {
            const size = std.fmt.count("{}{}", .{ x, y });
            var a = try v.Array(u8).init(M.DEFAULT_ALLOC, size);
            _ = try std.fmt.bufPrint(a.slice(), "{}{}", .{ x, y });
            x.destroy(M.DEFAULT_ALLOC);
            y.destroy(M.DEFAULT_ALLOC);
            try self.values.append(v.Value.from_str(a));
        } else {
            return error.TypeError;
        }
    }
};
