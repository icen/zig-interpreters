const c = @import("./chunk.zig");
const d = @import("./defs.zig");
const p = @import("./parser.zig");
const l = @import("./lex.zig");
const v = @import("./value.zig");

pub fn compile(chunk: *c.Chunk, parser: *p.Parser) !void {
    parser.advance();
    try parser.expression();
    parser.consume(l.Type.EOF, "Expect end of expression.");

    try end(chunk, parser.curr.line);

    if (parser.errored) {
        return error.ParseError;
    }
}

fn end(chunk: *c.Chunk, line: usize) !void {
    try chunk.instruction(line, d.Op.ret);
}
