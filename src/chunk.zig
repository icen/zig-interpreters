const std = @import("std");
const v = @import("./value.zig");
const defs = @import("./defs.zig");
const Code = defs.Code;
const Op = defs.Op;

pub const Chunk = struct {
    code: std.ArrayList(Code),
    constants: std.ArrayList(v.Value),
    lines: std.ArrayList(usize),

    pub fn init(alloc: std.mem.Allocator) Chunk {
        return Chunk{ .code = std.ArrayList(Code).init(alloc), .constants = std.ArrayList(v.Value).init(alloc), .lines = std.ArrayList(usize).init(alloc) };
    }

    pub fn deinit(self: Chunk) void {
        self.code.deinit();
        self.constants.deinit();
        self.lines.deinit();
    }

    pub fn instruction(self: *Chunk, line: usize, op: Op) !void {
        try self.lines.append(line);
        try self.code.append(.{ .opcode = op });
    }

    pub fn constant(self: *Chunk, line: usize, val: v.Value) error{ TooManyConstants, OutOfMemory }!void {
        if (self.constants.items.len >= 255) {
            return error.TooManyConstants;
        }
        try self.instruction(line, .constant);

        try self.lines.append(line);
        var i: usize = 0;
        while (i < self.constants.items.len) {
            if (v.Value.eq(self.constants.items[i], val)) {
                break;
            }
            i += 1;
        }
        try self.code.append(.{ .index = @intCast(i) });
        try self.constants.append(val);
    }

    pub fn clear(self: *Chunk) void {
        self.code.resize(0) catch unreachable;
        self.constants.resize(0) catch unreachable;
        self.lines.resize(0) catch unreachable;
    }
};
