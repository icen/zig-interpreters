const std = @import("std");
const l = @import("lex.zig");
const c = @import("./chunk.zig");
const v = @import("./value.zig");
const d = @import("./defs.zig");
const M = @import("./main.zig");

const Precedence = enum(u8) { NONE, BIND, OR, AND, EQ, CMP, TERM, FACTOR, UNARY, CALL, PRIMARY };

pub const Parser = struct {
    lex: l.Lexer,
    chunk: *c.Chunk,
    curr: l.Token = undefined,
    prev: l.Token = undefined,
    errored: bool = false,
    panic: bool = false,

    pub fn expression(self: *Parser) ParseErrors!void {
        try self.prec(Precedence.BIND);
    }

    fn unary(self: *Parser) ParseErrors!void {
        var op: l.Type = self.prev.ty;
        var line = self.prev.line;
        try self.prec(Precedence.UNARY);
        switch (op) {
            .DASH => try self.chunk.instruction(line, d.Op.neg),
            .BANG => try self.chunk.instruction(line, d.Op.not),
            .PRINT => try self.chunk.instruction(line, d.Op.print),
            else => unreachable,
        }
    }

    fn binary(self: *Parser) ParseErrors!void {
        var op = self.prev.ty;
        var line = self.prev.line;
        var rule = ruleFor(op);
        try self.prec(@enumFromInt(1 + @intFromEnum(rule.prec)));

        switch (op) {
            .PLUS => try self.chunk.instruction(line, .add),
            .DASH => try self.chunk.instruction(line, .sub),
            .STAR => try self.chunk.instruction(line, .mul),
            .SLASH => try self.chunk.instruction(line, .div),
            .EQ_EQ => try self.chunk.instruction(line, .eq),
            .LT => try self.chunk.instruction(line, .lt),
            .GT => try self.chunk.instruction(line, .gt),
            .BANG_EQ => {
                try self.chunk.instruction(line, .eq);
                try self.chunk.instruction(line, .not);
            },
            .LEQ => {
                try self.chunk.instruction(line, .gt);
                try self.chunk.instruction(line, .not);
            },
            .GEQ => {
                try self.chunk.instruction(line, .lt);
                try self.chunk.instruction(line, .not);
            },
            else => unreachable,
        }
    }

    fn group(self: *Parser) ParseErrors!void {
        try self.expression();
        self.consume(l.Type.RIGHT_PAREN, "Expected ')'");
    }

    fn number(self: *Parser) ParseErrors!void {
        try self.chunk.constant(self.prev.line, v.Value.from_f64(try std.fmt.parseFloat(f64, self.prev.src)));
    }

    fn string(self: *Parser) ParseErrors!void {
        try self.chunk.constant(self.prev.line, v.Value.from_str(try v.Array(u8).from_slice(self.prev.src, M.DEFAULT_ALLOC)));
    }

    fn literal(self: *Parser) ParseErrors!void {
        switch (self.prev.ty) {
            .TRUE => try self.chunk.constant(self.prev.line, v.Value.from_bool(true)),
            .FALSE => try self.chunk.constant(self.prev.line, v.Value.from_bool(false)),
            .NIL => try self.chunk.constant(self.prev.line, v.NIL),
            else => unreachable,
        }
    }

    fn prec(self: *Parser, precedence: Precedence) ParseErrors!void {
        self.advance();
        var rule = ruleFor(self.prev.ty);
        if (rule.prefix == null) {
            std.debug.print("{any}\n", .{self.prev});
            self.errorAt("Expect expression");
            return;
        }

        try rule.prefix.?(self);

        while (@intFromEnum(precedence) <= @intFromEnum(ruleFor(self.curr.ty).prec)) {
            self.advance();
            rule = ruleFor(self.prev.ty);
            if (rule.infix != null) {
                try rule.infix.?(self);
            }
        }
    }

    pub fn advance(self: *Parser) void {
        self.prev = self.curr;
        while (true) {
            self.curr = self.lex.next();
            if (self.curr.ty != l.Type.ERROR) break;
            self.errorAt(self.curr.src);
        }
    }

    fn errorAt(self: *Parser, msg: []const u8) void {
        if (self.panic) return;
        self.panic = true;
        std.debug.print("[{}:{}] Error: {s}\n", .{ self.curr.line, self.curr.column, msg });
        self.errored = true;
    }

    pub fn consume(self: *Parser, ty: l.Type, msg: []const u8) void {
        if (self.curr.ty == ty) {
            self.advance();
        } else {
            std.debug.print("{}!={}\n", .{ ty, self.curr.ty });
            self.errorAt(msg);
        }
    }
};

const ParseErrors = error{ ParseFloatError, InvalidCharacter, OutOfMemory, TooManyConstants };
const ParseRule: type = ?*const fn (*Parser) ParseErrors!void;
const Rule = struct { prec: Precedence = .NONE, prefix: ParseRule = null, infix: ParseRule = null };
fn ruleFor(op: l.Type) Rule {
    return switch (op) {
        .LEFT_PAREN => Rule{
            .prefix = &Parser.group,
        },
        .TRUE, .FALSE, .NIL => Rule{ .prefix = &Parser.literal },
        .DASH => Rule{ .prefix = &Parser.unary, .infix = &Parser.binary, .prec = .TERM },
        .PLUS => Rule{ .infix = &Parser.binary, .prec = .TERM },
        .SLASH, .STAR => Rule{ .infix = &Parser.binary, .prec = Precedence.FACTOR },
        .BANG, .PRINT => Rule{ .prefix = &Parser.unary },
        .NUM => Rule{ .prefix = &Parser.number },
        .STR => Rule{ .prefix = &Parser.string },
        .EQ_EQ, .BANG_EQ => Rule{ .infix = &Parser.binary, .prec = Precedence.EQ },
        .LT, .LEQ, .GT, .GEQ => Rule{ .infix = &Parser.binary, .prec = Precedence.CMP },
        else => Rule{},
    };
}
