pub const Op = enum(u8) {
    trap,
    ret,
    constant,
    add,
    sub,
    mul,
    div,
    neg,
    print,
    not,
    and_,
    or_,
    eq,
    gt,
    lt,
};

pub const Code = packed union { opcode: Op, index: u8 };
