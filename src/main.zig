const std = @import("std");
const v = @import("./value.zig");
const VM = @import("./vm.zig").VM;
const Chunk = @import("./chunk.zig").Chunk;
const lex = @import("./lex.zig");
const p = @import("./parser.zig");
const compiler = @import("./compiler.zig");

pub const DEFAULT_ALLOC: std.mem.Allocator = std.heap.c_allocator;

pub fn main() !void {
    var args = std.ArrayList([]const u8).init(DEFAULT_ALLOC);
    defer args.deinit();
    var args_iter = std.process.args();
    while (true) {
        const arg = args_iter.next();
        if (arg == null) break;
        try args.append(arg.?);
    }

    var vm = try VM.init(std.heap.c_allocator);
    defer vm.deinit();

    if (args.items.len == 1) return try repl(DEFAULT_ALLOC, &vm);
    if (args.items.len == 2) return try run(DEFAULT_ALLOC, &vm, args.items[1]);

    std.log.err("Usage: lox [path]\n", .{});
    std.process.exit(64);
}

fn repl(comptime alloc: std.mem.Allocator, vm: *VM) !void {
    var chunk = Chunk.init(alloc);
    defer chunk.deinit();
    var buf: std.BoundedArray(u8, 1024) = .{};
    var reader = std.io.getStdIn().reader();
    while (true) {
        var out = std.io.getStdOut().writer();
        try out.print("< ", .{});
        try buf.resize(0);
        chunk.clear();
        if (reader.streamUntilDelimiter(buf.writer(), '\n', 1024)) |_| {} else |err| {
            if (err == error.EndOfStream) {
                break;
            }
            return err;
        }

        var lexer: lex.Lexer = .{ .src = buf.slice() };
        var parser: p.Parser = .{ .lex = lexer, .chunk = &chunk };

        if (compiler.compile(&chunk, &parser)) |_| {} else |err| {
            if (err == error.ParseError) {
                continue;
            }
            return err;
        }

        disassemble(&chunk, "ouput");
        if (vm.interpret(&chunk)) |_| {
            const last = vm.values.pop();
            try out.print("> {}\n", .{last});
            last.destroy(DEFAULT_ALLOC);
        } else |err| {
            try out.print("> {}\n", .{err});
        }
    }
}

fn run(comptime alloc: std.mem.Allocator, vm: *VM, path: []const u8) !void {
    const src = try std.fs.cwd().readFileAlloc(alloc, path, std.math.maxInt(usize));
    defer alloc.free(src);
    var chunk = Chunk.init(alloc);
    defer chunk.deinit();

    var lexer: lex.Lexer = .{ .src = src };
    var parser: p.Parser = .{ .lex = lexer, .chunk = &chunk };

    try compiler.compile(&chunk, &parser);
    try vm.interpret(&chunk);
}

fn disassemble(chunk: *Chunk, name: []const u8) void {
    std.debug.print("== {s} ==\n", .{name});
    var offset: usize = 0;
    while (offset < chunk.code.items.len) {
        offset = decode(chunk, offset);
    }
}

fn decode(chunk: *Chunk, offset: usize) usize {
    const op = chunk.code.items[offset].opcode;
    const line = chunk.lines.items[offset];
    return switch (op) {
        .constant => {
            const ix = chunk.code.items[offset + 1].index;
            std.debug.print("{d: >2}: {d:0>4} {s:9} {} ({})\n", .{ line, offset, @tagName(op), ix, chunk.constants.items[ix] });
            return offset + 2;
        },
        else => {
            std.debug.print("{d: >2}: {d:0>4} {s:9}\n", .{ line, offset, @tagName(op) });
            return offset + 1;
        },
    };
}
