const std = @import("std");
pub const Type = enum(u8) { float, int, array, string, nil, boolean, class, instance };
pub fn Array(comptime T: type) type {
    if (T != u8 and T != i32 and T != Value) {
        unreachable;
    }

    return extern struct {
        len: usize,
        start: [1]T,

        pub fn init(alloc: std.mem.Allocator, len: usize) !*Array(T) {
            const mem = try alloc.alignedAlloc(u8, 8, @sizeOf(usize) + len * @sizeOf(T));
            const arr: *Array(T) = @alignCast(@ptrCast(mem.ptr));
            arr.len = len;
            return arr;
        }

        pub fn from_slice(input: []const T, alloc: std.mem.Allocator) !*Array(T) {
            const arr = try Array(T).init(alloc, input.len);
            @memcpy(arr.slice(), input);
            return arr;
        }

        pub fn slice(self: *Array(T)) []T {
            return (@as([*]T, @ptrCast(&self.start)))[0..self.len];
        }

        pub fn deinit(self: *Array(T), alloc: std.mem.Allocator) void {
            const ptr = @as([*]u8, @ptrCast(self));
            const as_slice: []u8 align(@alignOf(Array(T))) = @alignCast(@as([]u8, ptr[0 .. 8 + self.len * @sizeOf(T)]));
            alloc.free(as_slice);
        }
    };
}

pub const Class = struct { super: ?*Class = null, name: []const u8 };
pub const Instance = struct { class: *Class };

pub const NIL: Value = Value{ .repr = @bitCast(@as(u64, 0x7FFC_0000_0000_0000)) };
pub const Value = packed struct {
    repr: f64,

    pub inline fn from_f64(x: f64) Value {
        return Value{ .repr = x };
    }

    pub inline fn from_i32(x: i32) Value {
        return Value{ .repr = @bitCast(@as(u64, @intCast(x)) | 0x7FF9_0000_0000_0000) };
    }

    pub inline fn from_array(x: *Array(i32)) Value {
        return Value{ .repr = @bitCast(@intFromPtr(x) | 0x7FFA_0000_0000_0000) };
    }

    pub inline fn from_str(x: *Array(u8)) Value {
        return Value{ .repr = @bitCast(@intFromPtr(x) | 0x7FFB_0000_0000_0000) };
    }

    pub inline fn from_bool(x: bool) Value {
        return Value{ .repr = @bitCast(@as(u64, @intFromBool(x)) | 0x7FFD_0000_0000_0000) };
    }

    pub inline fn from_class(x: *Class) Value {
        return Value{ .repr = @bitCast(@intFromPtr(x) | 0x7FFE_0000_0000_0000) };
    }

    pub inline fn from_instance(x: *Instance) Value {
        return Value{ .repr = @bitCast(@intFromPtr(x) | 0x7FFF_0000_0000_0000) };
    }

    pub inline fn get_type(self: Value) Type {
        if (!std.math.isNan(self.repr)) {
            return .float;
        }

        const flag = (@as(u64, @bitCast(self.repr)) >> 48) & 0x07;
        return switch (flag) {
            0 => Type.float,
            1 => Type.int,
            2 => Type.array,
            3 => Type.string,
            4 => Type.nil,
            5 => Type.boolean,
            6 => Type.class,
            7 => Type.instance,
            else => unreachable,
        };
    }

    pub inline fn is(self: Value, comptime T: type) bool {
        if (T == f64) return self.get_type() == .float;
        if (T == i32) return self.get_type() == .int;
        if (T == *Array(i32)) return self.get_type() == .array;
        if (T == *Array(u8)) return self.get_type() == .string;
        if (T == bool) return self.get_type() == .boolean;
        if (T == *Class) return self.get_type() == .class;
        if (T == *Instance) return self.get_type() == .instance;
        if (T == void) return self.get_type() == .nil;
        return false;
    }

    pub inline fn as(self: Value, comptime T: type) T {
        switch (T) {
            f64 => {
                return self.repr;
            },
            i32 => {
                return @as(i32, @intCast(@as(u64, @bitCast(self.repr)) & ~@as(u64, 0x7FF9_0000_0000_0000)));
            },
            (*Array(u8)) => {
                return @ptrFromInt(@as(u64, @bitCast(self.repr)) & ~@as(u64, 0x7FFB_0000_0000_0000));
            },
            (*Array(i32)) => {
                return @ptrFromInt(@as(u64, @bitCast(self.repr)) & ~@as(u64, 0x7FFA_0000_0000_0000));
            },
            (*Class) => {
                return @ptrFromInt(@as(u64, @bitCast(self.repr)) & ~@as(u64, 0x7FFE_0000_0000_0000));
            },
            (*Instance) => {
                return @ptrFromInt(@as(u64, @bitCast(self.repr)) & ~@as(u64, 0x7FFF_0000_0000_0000));
            },
            bool => {
                return 1 == (@as(u64, @bitCast(self.repr)) & 1);
            },
            else => {
                std.debug.print("{}", .{T});
                unreachable;
            },
        }
    }

    pub fn eq(x: Value, y: Value) bool {
        if (x.repr == y.repr) return true;
        if (x.get_type() != y.get_type()) return false;
        switch (x.get_type()) {
            .nil => unreachable,
            .boolean, .int, .float, .instance, .class => return false,
            .array => return std.mem.eql(i32, x.as(*Array(i32)).slice(), y.as(*Array(i32)).slice()),
            .string => return std.mem.eql(u8, x.as(*Array(u8)).slice(), y.as(*Array(u8)).slice()),
        }
    }

    pub fn format(self: Value, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        switch (self.get_type()) {
            .nil => try writer.print("nil", .{}),
            .boolean => try writer.print("{}", .{self.as(bool)}),
            .float => try writer.print("{d}", .{self.as(f64)}),
            .int => try writer.print("{d}", .{self.as(i32)}),
            .array => try writer.print("{any}", .{self.as(*Array(i32)).slice()}),
            .string => try writer.print("{s}", .{self.as(*Array(u8)).slice()}),
            .class => try writer.print("<class {s}>", .{self.as(*Class).name}),
            .instance => try writer.print("<instance {s}>", .{self.as(*Instance).class.name}),
        }
    }

    pub inline fn destroy(self: Value, alloc: std.mem.Allocator) void {
        if (self.is(*Array(u8))) {
            self.as(*Array(u8)).deinit(alloc);
        } else if (self.is(*Array(i32))) {
            self.as(*Array(i32)).deinit(alloc);
        } else if (self.is(*Class)) {
            alloc.destroy(self.as(*Class));
        } else if (self.is(*Instance)) {
            alloc.destroy(self.as(*Instance));
        }
    }
};

pub fn truthy(v: Value) bool {
    return !v.is(void) and (v.is(bool) and v.as(bool)) or (v.is(f64) and v.as(f64) != 0);
}

test "nil" {
    try std.testing.expect(Type.nil == NIL.get_type());
}

test "floats" {
    try std.testing.expect(Type.float == Value.from_f64(1.0).get_type());
    try std.testing.expect(Value.from_f64(1.0).as(f64) == 1.0);
    try std.testing.expect(Type.float == Value.from_f64(std.math.nan_f64).get_type());
}

test "ints" {
    try std.testing.expect(Type.int == Value.from_i32(1).get_type());
    try std.testing.expect(1 == Value.from_i32(1).as(i32));
}

test "arrays" {
    const arr: *Array(i32) = try Array(i32).init(std.testing.allocator, 3);
    defer arr.deinit(std.testing.allocator);
    arr.slice()[0] = 1;
    arr.slice()[1] = 2;
    arr.slice()[2] = 3;
    try std.testing.expect(Type.array == Value.from_array(arr).get_type());
    const other = Value.from_array(arr).as(*Array(i32));
    try std.testing.expect((other.slice()[0] + other.slice()[1] + other.slice()[2]) == 6);
}

test "strings" {
    const str: *Array(u8) = try Array(u8).from_slice("abc", std.testing.allocator);
    defer str.deinit(std.testing.allocator);
    const val = Value.from_str(str);
    try std.testing.expect(Type.string == val.get_type());
    const out = val.as(*Array(u8));
    try std.testing.expect(str == out);
    try std.testing.expect(std.mem.eql(u8, "abc", out.slice()));
}

test "bools" {
    try std.testing.expect(Type.boolean == Value.from_bool(true).get_type());
    try std.testing.expect(Type.boolean == Value.from_bool(false).get_type());
    try std.testing.expect(Value.from_bool(true).as(bool));
    try std.testing.expect(!Value.from_bool(false).as(bool));
}

test "classes" {
    const class = try std.testing.allocator.create(Class);
    defer std.testing.allocator.destroy(class);
    const instance = try std.testing.allocator.create(Instance);
    defer std.testing.allocator.destroy(instance);
    try std.testing.expect(.class == Value.from_class(class).get_type());
    try std.testing.expect(.instance == Value.from_instance(instance).get_type());
    try std.testing.expect(class == Value.from_class(class).as(*Class));
    try std.testing.expect(instance == Value.from_instance(instance).as(*Instance));
}
