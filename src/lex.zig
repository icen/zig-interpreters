const std = @import("std");
const Chunk = @import("./chunk.zig").Chunk;
const Op = @import("./defs.zig").Op;

pub const Type = enum { LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE, COMMA, DOT, DASH, PLUS, SEMI, SLASH, STAR, BANG, BANG_EQ, EQ, EQ_EQ, GT, GEQ, LT, LEQ, ID, STR, NUM, AND, CLASS, ELSE, FALSE, FOR, FUN, IF, NIL, OR, PRINT, RET, SUPER, THIS, TRUE, VAR, WHILE, EOF, ERROR };
pub const Token = struct { line: u32, column: u32, pos: usize, ty: Type, src: []const u8 };

pub const Lexer = struct {
    src: []const u8,
    pos: usize = 0,
    line: u32 = 0,
    column: u32 = 0,

    pub fn next(self: *Lexer) Token {
        while (self.bounds(0) and std.ascii.isWhitespace(self.src[self.pos])) {
            self.advance(1);
        }
        if (self.src.len == self.pos) return self.emit(Type.EOF, 0);
        switch (self.src[self.pos]) {
            '(' => return self.emit(Type.LEFT_PAREN, 1),
            ')' => return self.emit(Type.RIGHT_PAREN, 1),
            '{' => return self.emit(Type.RIGHT_BRACE, 1),
            '}' => return self.emit(Type.RIGHT_BRACE, 1),
            ';' => return self.emit(Type.SEMI, 1),
            ',' => return self.emit(Type.COMMA, 1),
            '.' => return self.emit(Type.DOT, 1),
            '-' => return self.emit(Type.DASH, 1),
            '+' => return self.emit(Type.PLUS, 1),
            '*' => return self.emit(Type.STAR, 1),
            '/' => if (self.match('/')) {
                while (self.bounds(0) and self.src[self.pos] != '\n')
                    self.advance(1);
                return self.next();
            } else return self.emit(Type.SLASH, 1),
            '!' => return if (self.match('=')) self.emit(Type.BANG_EQ, 2) else self.emit(Type.BANG, 1),
            '=' => return if (self.match('=')) self.emit(Type.EQ_EQ, 2) else self.emit(Type.EQ, 1),
            '<' => return if (self.match('=')) self.emit(Type.LEQ, 2) else self.emit(Type.LT, 1),
            '>' => return if (self.match('=')) self.emit(Type.GEQ, 2) else self.emit(Type.GT, 1),
            '"' => return self.string(),
            else => {
                if (std.ascii.isDigit(self.src[self.pos])) return self.number();
                if (std.ascii.isAlphabetic(self.src[self.pos]) or self.src[self.pos] == '_') return self.ident();
            },
        }
        return self.err("Unknown character");
    }

    fn string(self: *Lexer) Token {
        var len: usize = 0;
        var esc = false;
        self.advance(1);
        while (self.bounds(len)) {
            switch (self.src[self.pos + len]) {
                '"' => if (esc) {
                    esc = false;
                } else {
                    var tok = self.emit(Type.STR, len);
                    self.advance(1);
                    return tok;
                },
                '\\' => {
                    esc = !esc;
                },
                else => {
                    esc = false;
                },
            }
            len += 1;
        }
        return self.err("Unterminated string");
    }

    fn number(self: *Lexer) Token {
        var len: usize = 0;
        while (self.bounds(len) and std.ascii.isDigit(self.src[self.pos + len])) len += 1;
        if (self.bounds(len) and self.src[self.pos + len] == '.' and std.ascii.isDigit(self.src[self.pos + len + 1])) {
            len += 1;
            while (self.bounds(len) and std.ascii.isDigit(self.src[self.pos + len])) len += 1;
        }
        return self.emit(Type.NUM, len);
    }

    fn ident(self: *Lexer) Token {
        var len: usize = 0;
        while (self.bounds(len) and (std.ascii.isAlphanumeric(self.src[self.pos + len]) or self.src[self.pos + len] == '_')) len += 1;
        return self.emit(identType(self.src[self.pos .. self.pos + len]), len);
    }

    fn match(self: *Lexer, comptime exp: u8) bool {
        return self.bounds(0) and self.src[self.pos + 1] == exp;
    }

    fn err(self: *Lexer, msg: []const u8) Token {
        return .{ .line = self.line, .column = self.column, .pos = self.pos, .ty = Type.ERROR, .src = msg };
    }

    fn emit(self: *Lexer, ty: Type, len: usize) Token {
        const src = self.src[self.pos .. self.pos + len];
        const tok = .{ .line = self.line, .column = self.column, .pos = self.pos, .ty = ty, .src = src };
        self.advance(len);
        return tok;
    }

    fn advance(self: *Lexer, len: usize) void {
        for (self.src[self.pos .. self.pos + len]) |c| {
            self.pos += 1;
            if (c == '\n') {
                self.line += 1;
                self.column = 0;
            } else {
                self.column += 1;
            }
        }
    }

    inline fn bounds(self: *Lexer, len: usize) bool {
        return self.pos + len < self.src.len;
    }
};

const KEYWORDS: [16]Type = [_]Type{ .AND, .CLASS, .ELSE, .FALSE, .FOR, .FUN, .IF, .NIL, .OR, .PRINT, .RET, .SUPER, .THIS, .TRUE, .VAR, .WHILE };
const LENGTHS: @Vector(16, u8) = @Vector(16, u8){ 3, 5, 4, 5, 3, 3, 2, 3, 2, 5, 6, 5, 4, 4, 3, 5 };
const SLICES: [6]@Vector(16, u8) = [_]@Vector(16, u8){ @Vector(16, u8){ 'a', 'c', 'e', 'f', 'f', 'f', 'i', 'n', 'o', 'p', 'r', 's', 't', 't', 'v', 'w' }, @Vector(16, u8){ 'n', 'l', 'l', 'a', 'o', 'u', 'f', 'i', 'r', 'r', 'e', 'u', 'h', 'r', 'a', 'h' }, @Vector(16, u8){ 'd', 'a', 's', 'l', 'r', 'n', ' ', 'l', ' ', 'i', 't', 'p', 'i', 'u', 'r', 'i' }, @Vector(16, u8){ ' ', 's', 'e', 's', ' ', ' ', ' ', ' ', ' ', 'n', 'u', 'e', 's', 'e', ' ', 'l' }, @Vector(16, u8){ ' ', 's', ' ', 'e', ' ', ' ', ' ', ' ', ' ', 't', 'r', 'r', ' ', ' ', ' ', 'e' }, @Vector(16, u8){ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'n', ' ', ' ', ' ', ' ', ' ' } };
fn identType(tok: []const u8) Type {
    if (tok.len > SLICES.len) {
        return .ID;
    }
    var avail: u16 = ~@as(u16, 0) & @as(u16, @bitCast(LENGTHS == @as(@Vector(16, u8), @splat(@as(u8, @intCast(tok.len))))));
    var pos: u8 = 0;
    while (pos < tok.len and avail != 0) {
        avail &= @as(u16, @bitCast(SLICES[pos] == @as(@Vector(16, u8), @splat(tok[pos]))));
        pos += 1;
    }
    if (@popCount(avail) == 1) {
        return KEYWORDS[15 - @clz(avail)];
    }
    return .ID;
}

test "ident types" {
    try std.testing.expect(.AND == identType("and"));
    try std.testing.expect(.CLASS == identType("class"));
    try std.testing.expect(.ELSE == identType("else"));
    try std.testing.expect(.FALSE == identType("false"));
    try std.testing.expect(.FOR == identType("for"));
    try std.testing.expect(.FUN == identType("fun"));
    try std.testing.expect(.IF == identType("if"));
    try std.testing.expect(.NIL == identType("nil"));
    try std.testing.expect(.OR == identType("or"));
    try std.testing.expect(.PRINT == identType("print"));
    try std.testing.expect(.RET == identType("return"));
    try std.testing.expect(.SUPER == identType("super"));
    try std.testing.expect(.THIS == identType("this"));
    try std.testing.expect(.TRUE == identType("true"));
    try std.testing.expect(.VAR == identType("var"));
    try std.testing.expect(.WHILE == identType("while"));
    try std.testing.expect(.ID == identType("iff"));
    try std.testing.expect(.ID == identType("clas"));
    try std.testing.expect(.ID == identType("definitelyAnIdent"));
}
